import React, { useEffect, useState } from "react";

export default function ConferenceForm(props) {

    const [locations, setLocations] = useState([]);

    const [name, setName] = useState("");
    const [starts, setStarts] = useState("");
    const [ends, setEnds] = useState("");
    const [maxPresentations, setMaxPresentations] = useState("");
    const [maxAttendees, setMaxAttendees] = useState("");
    const [description, setDescription] = useState("");
    const [location, setLocation] = useState("");

    const handleNameChange = event => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartsChange = event => {
        const value = event.target.value;
        setStarts(value);
    }

    const handleEndsChange = event => {
        const value = event.target.value;
        setEnds(value);
    }

    const handleMaxPresentationsChange = event => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendeesChange = event => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleDescriptionChange = event => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleLocationChange = event => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();

            console.log(newConference);
            setName('');
            setStarts('');
            setEnds('');
            setMaxPresentations('');
            setMaxAttendees('');
            setDescription('');
            setLocation('');
        }
    }


    const fetchData = async () => {

        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name"
                                className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <label htmlFor="datefield" className="small">Starts</label>
                            <input value={starts} onChange={handleStartsChange} type="date" className="form-control" id="starts" name="starts" />
                        </div>
                        <div className="form-floating mb-3">
                            <label htmlFor="datefield" className="small">Ends</label>
                            <input value={ends} onChange={handleEndsChange} type="date" className="form-control" id="ends" name="ends" />
                        </div>

                        <div className="form-grou mb-3">
                            <label htmlFor="exampleFormControlTextarea1">Description</label>
                            <textarea value={description} onChange={handleDescriptionChange} className="form-control" id="description" name="description" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={maxPresentations} onChange={handleMaxPresentationsChange} placeholder="Maximum presentations" required type="text" name="max_presentations"
                                id="max_presentations" className="form-control" />
                            <label htmlFor="name">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="Maximum attendees" required type="text" name="max_attendees" id="max_attendees"
                                className="form-control" />
                            <label htmlFor="name">Maximum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>{location.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <p id="log"></p>
                </div>
            </div>
        </div>
    );
}
